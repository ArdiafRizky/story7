$(document).ready(function(){
    var dark = false;
    $("#theme").click(function(){
        $("body").toggleClass("dark-mode-bg");
        $("h1").toggleClass("dark-mode-fonts");
        $("#theme").toggleClass("dark-mode-button");
        if(dark==false) {
            $("#theme").text("Light Mode");
            dark = true;
        }
        else {
            $("#theme").text("Dark Mode")
            dark = false;
        }
    })
    $("#accordions").accordion({
        events: "click"
    });
});