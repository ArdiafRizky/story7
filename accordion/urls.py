from django.urls import path
from .views import getAccordionPage
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = "accordion"

urlpatterns = [
    path('',getAccordionPage,name="accordionPage"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += staticfiles_urlpatterns()