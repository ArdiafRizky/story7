from django.test import TestCase,Client
from django.urls import resolve
from . import views
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import unittest, time
from selenium.webdriver.common.keys import Keys
from datetime import date, time
from time import sleep
from django.test import LiveServerTestCase

# Create your tests here.
class TestAccordion(TestCase):

    def test_get_page(self):
        response = Client().get("/")
        self.assertEqual(response.status_code,200)

    def test_get_accordionPage(self):
        response = resolve("/")
        self.assertEqual(response.func, views.getAccordionPage)

    def test_get_accordion_template(self):
        response = Client().get("/")
        self.assertTemplateUsed(response, "accordion/accordionPage.html")

    def test_contains_content(self):
        response = Client().get("/")
        self.assertContains(response,"MY SEVENTH STORY")
        self.assertContains(response, "Activities")
        self.assertContains(response, "Experiences")
        self.assertContains(response, "Achievement")

class TestFunctionalAccordion(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(executable_path="./chromedriver", chrome_options=chrome_options)
        super(TestFunctionalAccordion,self).setUp()
        
    def tearDown(self):
        self.browser.quit()
        super(TestFunctionalAccordion,self).tearDown()

    def test_functionality(self):
        driver = self.browser
        driver.get(self.live_server_url)

        dark_btn = driver.find_element_by_id("theme")
        dark_btn.click()
        sleep(1.1)
        self.assertIn("dark-mode-bg",driver.page_source)
        dark_btn.click()
        self.assertNotIn("dark-mode-bg",driver.page_source)
        sleep(1.1)

