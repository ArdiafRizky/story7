from django.shortcuts import render
# Create your views here.

def getAccordionPage(request):
    return render(request,"accordion/accordionPage.html")
